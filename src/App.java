import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1();
        subTask2();
        subTask3();
        subTask4();
        subTask5();
        subTask6();
        subTask7();
        subTask8();
        subTask9();
        subTask10();

    }

    public static void subTask1() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(2);
        numbers.add(8);
        numbers.add(1);
        numbers.add(10);
        numbers.add(3);
        numbers.add(6);
        numbers.add(4);
        numbers.add(9);
        numbers.add(7);

        System.out.println("ArrayList ban đầu: " + numbers);

        // Sắp xếp các phần tử theo thứ tự tăng dần
        Collections.sort(numbers);

        System.out.println("ArrayList đã được sắp xếp: " + numbers);
    }

    public static void subTask2() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(2);
        numbers.add(8);
        numbers.add(1);
        numbers.add(10);
        numbers.add(3);
        numbers.add(6);
        numbers.add(4);
        numbers.add(9);
        numbers.add(7);
        System.out.println("ArrayList ban đầu: " + numbers);
        ArrayList<Integer> newNumbers = new ArrayList<>();

        // Lấy các số từ ArrayList ban đầu có giá trị từ 10 đến 100
        for (Integer number : numbers) {
            if (number >= 10 && number <= 100) {
                newNumbers.add(number);
            }
        }

        System.out.println("ArrayList mới chứa các số từ 10 đến 100: " + newNumbers);

    }

    public static void subTask3() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");

        System.out.println("ArrayList vừa tạo: " + colors);

        // Kiểm tra xem ArrayList có chứa màu vàng hay không
        if (colors.contains("Vàng")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }
    }

    public static void subTask4() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(2);
        numbers.add(8);
        numbers.add(1);
        numbers.add(10);
        numbers.add(3);
        numbers.add(6);
        numbers.add(4);
        numbers.add(9);
        numbers.add(7);

        System.out.println("ArrayList ban đầu: " + numbers);

        // Tính tổng của các số trong ArrayList
        int sum = 0;
        for (Integer number : numbers) {
            sum += number;
        }

        System.out.println("Tổng của các số trong ArrayList: " + sum);
    }

    public static void subTask5() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");

        System.out.println("ArrayList ban đầu: " + colors);

        // Xóa bỏ toàn bộ giá trị của ArrayList
        colors.clear();

        System.out.println("ArrayList sau khi xóa: " + colors);
    }

    public static void subTask6() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");
        colors.add("Hồng");
        colors.add("Xám");
        colors.add("Cam");
        colors.add("Tím");
        colors.add("Đen");

        System.out.println("ArrayList ban đầu: " + colors);

        // Hoán đổi ngẫu nhiên vị trí các phần tử trong ArrayList
        Collections.shuffle(colors);

        System.out.println("ArrayList sau khi hoán đổi: " + colors);
    }

    public static void subTask7() {
        ArrayList<String> colors = new ArrayList<>();

        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");
        colors.add("Hồng");
        colors.add("Xám");
        colors.add("Cam");
        colors.add("Tím");
        colors.add("Đen");

        System.out.println("ArrayList ban đầu: " + colors);

        // Đảo ngược vị trí các phần tử trong ArrayList
        Collections.reverse(colors);

        System.out.println("ArrayList sau khi đảo ngược: " + colors);

    }

    public static void subTask8() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");
        colors.add("Hồng");
        colors.add("Xám");
        colors.add("Cam");
        colors.add("Tím");
        colors.add("Đen");

        System.out.println("ArrayList ban đầu: " + colors);

        // Cắt một phần của ArrayList từ phần tử thứ 3 đến phần tử thứ 7
        List<String> subList = colors.subList(2, 7);

        System.out.println("List mới: " + subList);
    }

    public static void subTask9() {
        ArrayList<String> colors = new ArrayList<>();

        colors.add("Đỏ");
        colors.add("Xanh lá cây");
        colors.add("Vàng");
        colors.add("Xanh dương");
        colors.add("Trắng");
        colors.add("Hồng");
        colors.add("Xám");
        colors.add("Cam");
        colors.add("Tím");
        colors.add("Đen");

        System.out.println("ArrayList ban đầu: " + colors);

        // Hoán đổi vị trí của phần tử thứ 3 và phần tử thứ 7 trong ArrayList
        Collections.swap(colors, 2, 6);

        System.out.println("ArrayList sau khi hoán đổi: " + colors);
    }

    public static void subTask10() {
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        // Thêm 3 số vào mỗi ArrayList
        list1.add(1);
        list1.add(2);
        list1.add(3);

        list2.add(4);
        list2.add(5);
        list2.add(6);

        System.out.println("ArrayList 1: " + list1);
        System.out.println("ArrayList 2: " + list2);

        // Sao chép ArrayList 1 vào ArrayList 2
        List<Integer> copiedList = new ArrayList<>(list1);
        list2.addAll(copiedList);

        System.out.println("ArrayList 1 sau khi sao chép: " + list1);
        System.out.println("ArrayList 2 sau khi sao chép: " + list2);
    }

}
